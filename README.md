# RTD Designs for THz EH

Public Keysight ADS designs and EH models for THz EH circuits based on RTDs developed at Institute of Digital Communications (IDC) of Friedrich-Alexander University Erlangen-Nuremberg (FAU) and University of Duisburg-Essen (UDE).

## Content
| Folder | Content |
| ------ | ------ |
| [rtd_ads](rtd_ads)       | Developed Keysight ADS RTD Design Kits       |
| [ehModel_matlab](ehModel_matlab)       | Designed EH models of RTD-based EH circuits       |

## Publications
The material of this repo is utilized in the following authors' publications:

- N. Shanin, S. Clochiatti, K. M. Mayer, L. Cottatellucci, N. Weimann, and R. Schober, “Achievable rate-power tradeoff in THz SWIPT systems with resonant tunnelling diodes,” arXiv preprint arXiv:2307.06036, submitted to IEEE Trans. Commun., 2023, DOI: doi.org/10.48550/arXiv.2307.06036

- N. Shanin, S. Clochiatti, K. M. Mayer, L. Cottatellucci, N. Weimann, and R. Schober, “Resonant Tunneling Diode-Based THz SWIPT for Microscopic 6G IoT Devices ,” arXiv preprint arXiv:2305.03532, accepted to IEEE GC Wkshps, 2023, DOI: doi.org/10.48550/arXiv.2305.03532

- N. Shanin, S. Clochiatti, K. M. Mayer, L. Cottatellucci, N. Weimann and R. Schober, "Average Harvested Power in THz WPT Systems Employing Resonant-Tunnelling Diodes," in Proc. 6th Int. Workshop Mobile Terahertz Syst. (IWMTS), 2023, DOI: doi.org/10.1109/IWMTS58186.2023.10207782

## Bugs and Questions
For questions, inquiries, and bug reports, please contact the author by email: nikita.shanin@fau.de

## Copyright
This project is licensed under the Creative Commons Attribution Share Alike 4.0. [Learn more](https://creativecommons.org/licenses/by-nc-sa/4.0/)

You are welcome to copy, redistribute, and build upon the material of this repo for non-commerical use, as long as an appropriate credit is given. For scientific publications utilizing the provided EH models and RTD designs, please cite the following work:

_N. Shanin, S. Clochiatti, K. M. Mayer, L. Cottatellucci, N. Weimann, and R. Schober, “Achievable rate-power tradeoff in THz SWIPT systems with resonant tunnelling diodes,” arXiv preprint arXiv:2307.06036, submitted to IEEE Trans. Commun., 2023._
