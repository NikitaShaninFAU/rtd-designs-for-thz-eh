clear all

%In this document, we plot the EH models shown in Fig. 7 of the TCOM paper

%Instantaneous input power rho[mW]
rho = logspace(-2, 1, 1000);

%RTD design in [17]
psi_rtd = modelEH_rtd(rho*1e-3);

%Improved RTD design with higher Ubr
psi_improvedUbr= modelEH_improvUbr(rho*1e-3);

%Improved RTD design with lower Irev
psi_improvedIrev = modelEH_improvIrev(rho*1e-3);

%Plot the EH models
loglog(rho, psi_rtd*1e6, 'DisplayName', "RTD Model in [17]")
hold on
loglog(rho, psi_improvedUbr*1e6, 'DisplayName', "Improved design with higher U_{br}")
loglog(rho, psi_improvedIrev*1e6, 'DisplayName', "Improved design with lower I_{rev}")

xlabel('Instantaneous Input Power, \rho [mW]')
ylabel('Instantaneous Harvested Power, \psi(\rho) [\mu W]')

legend('Location', 'northwest')
