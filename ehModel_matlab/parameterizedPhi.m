function Pout = parameterizedPhi(rho, B, theta, alpha, beta, rho_n1, Phi_n1)

    Pout = B + (Phi_n1-B) .* ( 1 + (theta * (rho - rho_n1)).^alpha  ).^(-beta);

end
