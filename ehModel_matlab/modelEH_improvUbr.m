function [Pout] = modelEH_improvUbr(rho)
    
    rho1 = 4.1e-3;
    rho2 = 4.17e-3;
    rho3 = 6.18e-3;

    Pout = zeros(length(rho), 1);
    
    for i = 1:length(rho)

        input = rho(i);

        if input <= rho1        
            Pout(i) = parameterizedPhi(input, 3.6e-3, 241.6, 1.534, 0.289, 0, 0);
        elseif input < rho2
            Pout(i) = parameterizedPhi(input, 535e-6, 1692, 3.492, 10000, rho1, 6.47e-4);
        elseif input < rho3
            Pout(i) = parameterizedPhi(input, 2.85e-3, 294.8, 1.492, 0.2437, rho2, 5.36e-4);
        else
            Pout(i) = 0;
        end

    end
end