function [Pout] = modelEH_improvIrev(rho)
    
    rho1 = 2.1e-3;
    rho2 = 3e-3;

    Pout = zeros(length(rho), 1);
    
    for i = 1:length(rho)

        input = rho(i);

        if input <= rho1        
            Pout(i) = parameterizedPhi(input, 315e-6, 3580, 1.46, 0.527, 0, 0);
        elseif input < rho2
            Pout(i) = parameterizedPhi(input, 104e-6, 1100, 2.601, 0.703, rho1, 2.5e-4);
        else
            Pout(i) = 0;
        end

    end
end