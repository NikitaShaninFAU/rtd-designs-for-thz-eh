function [Pout] = modelEH_rtd(rho)
    
    rho1 = 1.8e-3;
    rho2 = 2.4e-3;

    Pout = zeros(length(rho2), 1);
    
    for i = 1:length(rho)

        input = rho(i);

        if input <= rho1        
            Pout(i) = parameterizedPhi(input, 71.6e-6, 2174.9, 1.432, 0.778, 0, 0);
        elseif input < rho2
            Pout(i) = parameterizedPhi(input, 25e-6, 956.8, 1.841, 0.445, rho1, 5.75e-5);
        else
            Pout(i) = 0;
        end

    end
end